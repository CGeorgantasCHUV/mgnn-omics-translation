import numpy as np
import pandas as pd
from sklearn.feature_selection import f_classif, SelectKBest
import pathlib


class TCGALoader:
    def __init__(self, data_path="data/TCGA", modality_names=None, filtering=False):
        self.data_path = pathlib.Path(data_path)
        cancer_list = [
            "aml",
            "breast",
            "colon",
            "gbm",
            "kidney",
            "liver",
            "lung",
            "melanoma",
            "ovarian",
            "sarcoma",
        ]
        self.cancers = {key: i for i, key in enumerate(cancer_list)}
        if not modality_names:
            self.modality_names = ["exp", "methy", "mirna"]
        else:
            self.modality_names = modality_names
        self.df_dict = dict()

        for mod in self.modality_names:
            df_list = []
            for group, grp_nbr in self.cancers.items():
                mod_path = self.data_path / "cancer" / group / mod
                if mod == "survival":
                    df = pd.read_csv(str(mod_path), sep="\t")
                else:
                    df = pd.read_csv(str(mod_path), sep=" ").T
                    if mod == "exp" or mod == "mirna":
                        df = np.log2(df + 1)

                df["Group"] = grp_nbr
                df.columns = df.columns.str.replace("-", ".")
                if df_list:
                    if len(df.columns) == len(df_list[0].columns):
                        df.columns = df_list[0].columns

                df_list.append(df)

            concat_df = pd.concat(df_list)

            concat_df.fillna(concat_df.mean(), inplace=True)
            self.df_dict[mod] = concat_df
        self.df_dict = reindex_intersection(
            self.df_dict
        )  # only includes samples that appear in all modalities
        self.df_dict = filter_columns(
            self.df_dict, filtering=filtering
        )  # remove low variance variables (mostly 0s)
        self.df_dict = split_gt(
            self.df_dict
        )  # each modality is a tuple of dataframes, (features, labels), this makes it easier to work with for unsup. tasks
        self.n_samples = self.df_dict[self.modality_names[0]][0].shape[0]

    def __call__(self):
        return self.df_dict


def select_most_relevant_features_global(df, gt_names, k):
    df_x = df.drop(columns=["Group"])
    df_x = df_x.drop(columns=gt_names)
    yc = df["Group"]
    fit = SelectKBest(f_classif, k=k).fit(df_x, yc)
    feature_names = fit.get_feature_names_out()
    return feature_names


def select_most_relevant_features(df, gt_names, k=5):
    selected_features = set()
    df_x = df.drop(columns=["Group"])
    df_x = df_x.drop(columns=gt_names)
    for diag_name in gt_names:
        yc = df[diag_name]
        fit = SelectKBest(f_classif, k=k).fit(df_x, yc)
        feature_names = fit.get_feature_names_out()
        selected_features = selected_features.union(feature_names)

    return list(selected_features)


def drop_correlated_features(df, threshold, method="pearson"):
    correlated_features = set()
    correlation_matrix = df.corr(method=method)
    for i in range(len(correlation_matrix.columns)):
        for j in range(i):
            if abs(correlation_matrix.iloc[i, j]) > threshold:
                colname = correlation_matrix.columns[i]
                correlated_features.add(colname)
    return df.drop(labels=correlated_features, axis=1)


def remove_overly_censored_features(df, threshold):
    """
    remove cols where % of values is above a specified threshold
    """
    df_temp = df.fillna(-1)
    f_len = len(df)
    c_value_series = (df_temp == -1).astype(int).sum(axis=0)
    c_value_series_bool = c_value_series < int(threshold * f_len)
    idx = c_value_series_bool[c_value_series_bool].index
    return df[idx]


def reindex_intersection(df_dict):
    index = None
    for key, df in df_dict.items():
        if index is None:
            index = df.index
        else:
            index = pd.Index.intersection(index, df.index)
    for key in df_dict:
        df_dict[key] = df_dict[key].reindex(index)
    return df_dict


def filter_columns(df_dict, filtering=False):
    for key, df in df_dict.items():
        df_std = (df.drop(["Group"], axis=1)).std(axis=0).sort_values(ascending=False)
        if filtering:
            std_idx = df_std[df_std > 0][0:6000].index
        else:
            std_idx = df_std[df_std > 0].index
        df_dict[key] = df.reindex(std_idx, axis="columns")
        df_dict[key]["Group"] = df["Group"]  # put back group

    return df_dict


def split_gt(df_dict):
    for key, df in df_dict.items():
        grp = df["Group"]
        df.drop(["Group"], inplace=True, axis=1)
        df_dict[key] = (df, grp)
    return df_dict


def generate_index(n_nodes, train_size, val_size=0):
    idx = np.arange(0, n_nodes)
    train_idx = np.random.choice(idx, train_size, replace=False)
    if val_size == 0:
        val_idx = None
        test_idx = np.random.choice(
            np.delete(idx, train_idx), n_nodes - train_size, replace=False
        )
    else:
        val_idx = np.random.choice(np.delete(idx, train_idx), val_size, replace=False)
        test_idx = np.delete(idx, np.concatenate((train_idx, val_idx)))
    return train_idx, val_idx, test_idx


def generate_features_and_targets_from_multimodal_dataset(
    multi_modality_dataset: dict, predict_modalities=["mirna"]
):
    df_dict_x = {
        k: v[0]
        for k, v in multi_modality_dataset.items()
        if k not in predict_modalities
    }

    df_x = pd.concat(df_dict_x.values(), axis=1)  # concat modalities
    targets = np.concatenate(
        [
            v[0].to_numpy()
            for k, v in multi_modality_dataset.items()
            if k in predict_modalities
        ],
        axis=1,
    )
    features = np.concatenate(
        [
            v[0].to_numpy()
            for k, v in multi_modality_dataset.items()
            if k not in predict_modalities
        ],
        axis=1,
    )
    return df_x, features, targets


def generate_graphs(graph_constructor, df_x, train_idx=None):
    graphs = graph_constructor(df_x)  # construct graph (on all samples)
    if train_idx is None:
        return graphs
    else:
        graphs_train = graph_constructor(df_x.iloc[train_idx])
        return graphs, graphs_train
