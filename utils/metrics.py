from sklearn import metrics
import numpy as np


def compute_average_metric_dict(metric_dict_list):
    keys = metric_dict_list[0].keys()
    avg_metric_dict = {}
    for key in keys:
        arr = np.array([metric_dict[key] for metric_dict in metric_dict_list])
        avg_metric_dict[key] = np.average(arr)
        avg_metric_dict[f"{key}_std"] = np.std(arr)

    return avg_metric_dict


def append_metric_dicts(metric_dict_list):
    keys = metric_dict_list[0].keys()
    avg_metric_dict = {}
    for key in keys:
        arr = np.array([metric_dict[key] for metric_dict in metric_dict_list])
        avg_metric_dict[key] = arr

    return avg_metric_dict


def print_metrics(metric_dict):
    for key, value in metric_dict.items():
        if isinstance(value, np.ndarray):
            print(f"{key} : {value}")
        else:
            print(f"{key} : {value:.3f}")


def compute_distance_metrics(x, x_hat, per_sample=False):

    metric_dict = {}

    if per_sample:
        metric_dict["MSE"] = metrics.mean_squared_error(
            x_hat.T, x.T, multioutput="raw_values"
        )
        metric_dict["l2"] = np.linalg.norm(x - x_hat, axis=-1)
        metric_dict["RMSE"] = metrics.mean_squared_error(
            x_hat.T, x.T, squared=False, multioutput="raw_values"
        )
        metric_dict["MAE"] = metrics.mean_absolute_error(
            x_hat.T, x.T, multioutput="raw_values"
        )
        metric_dict["MEDAE"] = metrics.median_absolute_error(
            x_hat.transpose(), x.transpose(), multioutput="raw_values"
        )
        metric_dict["R2"] = metrics.r2_score(
            x_hat.transpose(), x.transpose(), multioutput="raw_values"
        )
    else:
        metric_dict["MSE"] = metrics.mean_squared_error(x_hat, x)
        metric_dict["l2"] = np.linalg.norm(x - x_hat, axis=-1).mean()
        metric_dict["RMSE"] = metrics.mean_squared_error(x_hat, x, squared=False)
        metric_dict["MAE"] = metrics.mean_absolute_error(x_hat, x)
        metric_dict["MEDAE"] = metrics.median_absolute_error(
            x_hat.transpose(), x.transpose()
        )
        metric_dict["R2"] = metrics.r2_score(x_hat.transpose(), x.transpose())

    return metric_dict
