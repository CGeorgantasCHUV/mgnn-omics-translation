import argparse
from typing import Optional, List


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--dataset", nargs="?", default="tcga")
    parser.add_argument("--device", type=Optional[str], default=None, help="device")
    parser.add_argument("--sc", type=float, default=3, help="GCN self connection")

    parser.add_argument("--hid_units", type=int, default=512, help="hidden dimension")
    parser.add_argument("--latent_dim", type=int, default=256, help="hidden dimension")
    parser.add_argument("--lr", type=float, default=0.001, help="learning rate")
    parser.add_argument(
        "--train_ratio", type=float, default=0.8, help="train dataset ratio"
    )
    parser.add_argument(
        "--val_ratio",
        type=Optional[float],
        default=0.05,
        help="validation dataset ratio",
    )

    parser.add_argument(
        "--nb_epochs", type=int, default=15000, help="the number of epochs"
    )
    parser.add_argument(
        "--patience", type=int, default=3000, help="patience for early stopping"
    )
    parser.add_argument(
        "--milestones",
        type=List[int],
        default=[1500, 2000, 3000, 4000, 5000, 6000, 10000],
        help="milestones for learning rate scheduler",
    )
    parser.add_argument(
        "--n_clusters",
        type=int,
        default=10,
        help="number of clusters for feature clustering",
    )
    parser.add_argument("--experiment_name", type=str, default="test")

    return parser.parse_known_args()


def printConfig(args):
    arg_dict = {}
    for arg in vars(args):
        arg_dict[arg] = getattr(args, arg)
    print(arg_dict)
