import pathlib
import torch
import pickle as pkl
from pathlib import Path
from tqdm import tqdm
from utils.data_utils import (
    TCGALoader,
    generate_index,
    generate_features_and_targets_from_multimodal_dataset,
    generate_graphs,
)
from utils.parser import parse_args
from utils.metrics import compute_distance_metrics
from graphs.constructors import (
    FeatureClusterGraphConstructor,
    build_graph_data,
    GraphConstructorSNF,
)
from graphs.factories import GraphDatasetFactory
from ml.models import Decoder, VarEncoderDecoder, GraphEncoder
from ml.criterions import VAELoss, ReconstructionLoss
from ml.layers import VAEBridge


def train(
    model,
    dataset,
    criterion,
    test_criterion,
    optimiser,
    parameters,
    device,
    scheduler=None,
):
    model = model.to(device)
    features = dataset.features.to(device)
    labels = dataset.labels.to(device)
    adj_tensor = torch.stack(dataset.adj_list).to(device)

    if model.save_path is None:
        model_save_path = "saved_models/test.pkl"
    else:
        model_save_path = model.save_path

    print("Started training...")
    cnt_wait = 0
    best = 1e9
    best_dict = {}
    model.train()
    for i in tqdm(range(parameters.nb_epochs)):
        optimiser.zero_grad()
        outputs, mu, log_var = model(features, adj_tensor)
        loss = criterion(
            labels[dataset.idx_train],
            outputs[dataset.idx_train],
            mu[dataset.idx_train],
            log_var[dataset.idx_train],
        )

        loss.backward()
        optimiser.step()
        if scheduler is not None:
            scheduler.step()

        # eval every 5 epochs
        if i % 5 == 0:
            model.eval()
            outputs, mu, log_var = model(features, adj_tensor)

            if dataset.idx_val is not None:
                val_loss = test_criterion(
                    labels[dataset.idx_val].detach(), outputs[dataset.idx_val].detach()
                )
            else:
                test_loss = test_criterion(
                    labels[dataset.idx_test].detach(),
                    outputs[dataset.idx_test].detach(),
                )
                val_loss = test_loss

            if val_loss < best:
                best_dict = compute_distance_metrics(
                    labels[dataset.idx_test].detach().cpu().numpy(),
                    outputs[dataset.idx_test].detach().cpu().numpy(),
                )
                best_dict_val = compute_distance_metrics(
                    labels[dataset.idx_val].detach().cpu().numpy(),
                    outputs[dataset.idx_val].detach().cpu().numpy(),
                )
                print(best_dict_val)
                best = val_loss
                cnt_wait = 0

                torch.save(model.state_dict(), model_save_path)
            else:
                cnt_wait += 5
            if cnt_wait == parameters.patience:
                print("Early stopped!")
                break
            model.train()

    model.load_state_dict(torch.load(model_save_path))

    return best_dict


if __name__ == "__main__":

    args, _ = parse_args()

    n_clusters = args.n_clusters
    if not args.device:
        args.device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

    exp_name = args.experiment_name
    pathlib.Path("data/saved_models").mkdir(parents=True, exist_ok=True)
    pathlib.Path("data/saved_graphs").mkdir(parents=True, exist_ok=True)
    model_path = f"data/saved_models/{exp_name}.pkl"
    pkl_path = f"data/saved_graphs/{exp_name}.pkl"
    # save graphs by default and only build them if none exists for corresponding experiment name
    if Path(pkl_path).is_file():
        data_dict = pkl.load(open(pkl_path, "rb"))
    else:
        print("Builing graph, this may take a while")
        loader = TCGALoader(filtering=True)
        dataset = loader()
        n_samples = loader.n_samples
        train_idx, val_idx, test_idx = generate_index(
            n_nodes=n_samples,
            train_size=int(n_samples * args.train_ratio),
            val_size=int(n_samples * args.val_ratio),
        )
        graph_constructor = FeatureClusterGraphConstructor(
            n_clusters=n_clusters, sim_threshold=0.5
        )

        df_x, features, targets = generate_features_and_targets_from_multimodal_dataset(
            dataset, predict_modalities=["mirna"]
        )
        graphs, graphs_train = generate_graphs(
            graph_constructor, df_x, train_idx=train_idx
        )
        data_dict = build_graph_data(
            graphs, features, targets, train_idx, test_idx, val_idx, graphs_train
        )

        print("graph construction done")
        with open(pkl_path, "wb") as fp:
            pkl.dump(data_dict, fp)

    dataset = GraphDatasetFactory.from_dict(data_dict, args)  # generate dataset

    encoder = GraphEncoder(
        dataset.ft_size, args.hid_units, len(dataset.adj_list), dropout=0.05
    )
    decoder = Decoder(dataset.labels.shape[1], args.latent_dim, dropout=0.05).to(
        args.device
    )
    bridge = VAEBridge(args.hid_units, args.latent_dim, args.device)
    model = VarEncoderDecoder(encoder, bridge, decoder, model_path)

    optimiser = torch.optim.Adam(model.parameters(), lr=args.lr)
    scheduler = torch.optim.lr_scheduler.MultiStepLR(
        optimiser, milestones=args.milestones, gamma=0.5
    )
    criterion = VAELoss()
    test_criterion = ReconstructionLoss()

    best_dict = train(
        model,
        dataset,
        criterion,
        test_criterion,
        optimiser,
        args,
        args.device,
        scheduler,
    )
    print(best_dict)
