import torch
import torch.nn as nn
import torch.nn.functional as F
import functools

# taken from https://github.com/zhangxiaoyu11/OmiTrans
class FCBlock(nn.Module):
    def __init__(
        self,
        input_dim,
        output_dim,
        norm_layer=nn.BatchNorm1d,
        leaky_slope=0.2,
        dropout_p=0,
        activation=True,
        normalization=True,
        activation_name="leakyrelu",
        bias=True,
    ):
        """
        Construct a fully-connected block
        Parameters:
            input_dim (int)         -- the dimension of the input tensor
            output_dim (int)        -- the dimension of the output tensor
            norm_layer              -- normalization layer
            leaky_slope (float)     -- the negative slope of the Leaky ReLU activation function
            dropout_p (float)       -- probability of an element to be zeroed in a dropout layer
            activation (bool)       -- need activation or not
            normalization (bool)    -- need normalization or not
            activation_name (str)   -- name of the activation function used in the FC block
        """
        super(FCBlock, self).__init__()
        # Linear
        self.fc_block = [nn.Linear(input_dim, output_dim, bias=bias)]
        # Norm
        if normalization:
            # FC block doesn't support BatchNorm1d
            if (
                isinstance(norm_layer, functools.partial)
                and norm_layer.func == nn.BatchNorm1d
            ):
                norm_layer = nn.InstanceNorm1d
            self.fc_block.append(norm_layer(output_dim))
        # Dropout
        if 0 < dropout_p <= 1:
            self.fc_block.append(nn.Dropout(p=dropout_p))
        # LeakyReLU
        if activation:
            if activation_name.lower() == "leakyrelu":
                self.fc_block.append(
                    nn.LeakyReLU(negative_slope=leaky_slope, inplace=True)
                )
            elif activation_name.lower() == "tanh":
                self.fc_block.append(nn.Tanh())
            elif activation_name.lower() == "sigmoid":
                self.fc_block.append(nn.Sigmoid())
            else:
                raise NotImplementedError(
                    "Activation function [%s] is not implemented" % activation_name
                )

        self.fc_block = nn.Sequential(*self.fc_block)

    def forward(self, x):
        y = self.fc_block(x)
        return y


# taken from https://github.com/txWang/MOGONET/blob/main/models.py
class GraphConvolution(nn.Module):
    def __init__(self, in_features, out_features, bias=True):
        super().__init__()
        self.in_features = in_features
        self.out_features = out_features
        self.bias = None
        self.weight = nn.Parameter(torch.FloatTensor(in_features, out_features))
        if bias:
            self.bias = nn.Parameter(torch.FloatTensor(out_features))
        nn.init.xavier_normal_(self.weight.data)
        if self.bias is not None:
            self.bias.data.fill_(0.0)

    def forward(self, x, adj):
        support = torch.mm(x, self.weight)
        output = torch.sparse.mm(adj, support)
        if self.bias is not None:
            return output + self.bias
        else:
            return output


class GCN(nn.Module):
    def __init__(self, in_dim, hgcn_dim=256, dropout=0.1):
        super().__init__()
        self.fc = FCBlock(in_dim, hgcn_dim, bias=False)
        self.gc1 = GraphConvolution(hgcn_dim, hgcn_dim, bias=True)
        self.dropout = dropout

    def forward(self, x, adj):
        x2 = self.fc(x)
        x = self.gc1(x2, adj)
        x = F.dropout(x, self.dropout, training=self.training)
        x = x + x2
        x = torch.tanh(x)

        return x


class VAEBridge(nn.Module):
    def __init__(self, in_dim, out_dim, device) -> None:
        super().__init__()
        self.device = device
        self.lin_mu = nn.Linear(in_dim, out_dim)
        self.lin_sig = nn.Linear(in_dim, out_dim)

    def forward(self, x):
        mu = self.lin_mu(x)
        log_var = self.lin_sig(x)
        return mu, log_var

    def sample(self, mu, log_var):
        var = torch.exp(0.5 * log_var)
        eps = torch.rand_like(var).to(self.device)
        z = mu + var * eps
        return z
