import torch
import torch.nn as nn
import torch.nn.functional as F


class ReconstructionLoss(nn.Module):
    def __init__(self) -> None:
        super().__init__()

    def forward(self, x, x_hat):
        """assume x and x_hat are [n_samples, n_dim]"""
        return torch.linalg.norm(x - x_hat, dim=-1).mean()


def kl_gauss(mu, log_var):
    """kl divergence of gaussian distributions"""
    return -0.5 * torch.sum(1 + log_var - mu.pow(2) - log_var.exp())


def recon_loss(x, x_hat):
    return nn.functional.mse_loss(x, x_hat, reduction="sum")


class VAELoss(nn.Module):
    def __init__(self) -> None:
        super().__init__()

    def forward(self, x, x_hat, mu, log_var):
        kl = kl_gauss(mu, log_var)
        rec = recon_loss(x, x_hat)
        return kl + rec
