import torch
import torch.nn as nn
from ml.layers import GCN, FCBlock
import torch.nn.functional as F


class Encoder(nn.Module):
    def __init__(self, ft_size, hid_units):
        super(Encoder, self).__init__()
        self.lin1 = nn.Linear(ft_size, ft_size // 2)
        self.lin2 = nn.Linear(ft_size // 2, hid_units)
        self.act = nn.Tanh()

    def forward(self, x, adj_tensor=None):
        x = self.lin1(x)
        x = self.act(x)
        x = self.lin2(x)
        return x


class Decoder(nn.Module):
    def __init__(self, ft_size, hid_units, dropout=0.1):
        super(Decoder, self).__init__()
        self.layers = nn.Sequential(
            FCBlock(
                hid_units,
                ft_size // 2,
                dropout_p=dropout,
                activation_name="tanh",
                normalization=False,
            ),
            FCBlock(
                ft_size // 2,
                ft_size,
                dropout_p=dropout,
                activation=False,
                normalization=False,
            ),
        )

    def forward(self, x, adj_tensor=None):
        x = self.layers(x)
        return x


class VarEncoderDecoder(nn.Module):
    def __init__(self, encoder, bridge, decoder, save_path=None) -> None:
        super().__init__()
        self.encoder = encoder
        self.bridge = bridge
        self.decoder = decoder
        self.save_path = save_path

    def forward(self, features, adj_tensor):
        x = self.encoder(features, adj_tensor)
        mu, log_var = self.bridge(x)
        z = self.bridge.sample(mu, log_var)
        x = self.decoder(z, adj_tensor)
        return x, mu, log_var

    def embed(self, features, adj_tensor):
        return self.forward(features, adj_tensor).detach()


class GraphEncoder(nn.Module):
    def __init__(self, ft_size, hid_units, n_networks, dropout=0.1):
        super(GraphEncoder, self).__init__()
        self.in_size = ft_size
        self.gcn_list = nn.ModuleList(
            [GCN(self.in_size, hid_units, dropout=dropout) for _ in range(n_networks)]
        )
        self.w_list = nn.ModuleList(
            [
                FCBlock(
                    hid_units,
                    hid_units,
                    dropout_p=dropout,
                    bias=False,
                    activation_name="tanh",
                )
                for _ in range(n_networks)
            ]
        )
        self.y_list = nn.ModuleList(
            [
                FCBlock(
                    hid_units, 1, dropout_p=dropout, bias=False, activation_name="tanh"
                )
                for _ in range(n_networks)
            ]
        )

        self.att_act = nn.Softmax(dim=-1)

        for m in self.modules():
            self.weights_init(m)

    def weights_init(self, m):
        if isinstance(m, nn.Linear):
            torch.nn.init.xavier_uniform_(m.weight.data)
            if m.bias is not None:
                m.bias.data.fill_(0.0)

    def combine_att(self, h_list):
        h_combine_list = []
        for i, h in enumerate(h_list):
            h = self.w_list[i](h)
            h = self.y_list[i](h)
            h_combine_list.append(h)

        score = torch.cat(h_combine_list, -1)

        score = self.att_act(score)
        score = torch.unsqueeze(score, -1)
        h = torch.stack(h_list, dim=1)
        h = score * h
        h = torch.sum(h, dim=1)
        return h

    def forward(self, seq, adj_tensor):
        h_1_list = []
        for i in range(adj_tensor.shape[0]):
            adj = adj_tensor[i]
            h_1 = torch.squeeze(self.gcn_list[i](seq, adj))
            h_1_list.append(h_1)
        h = self.combine_att(h_1_list)
        return h

    def embed(self, seq, adj_tensor):
        h_1_list = []
        for i in range(adj_tensor.shape[0]):
            adj = adj_tensor[i]
            h_1 = torch.squeeze(self.gcn_list[i](seq, adj))
            h_1_list.append(h_1)
        h = self.combine_att(h_1_list)
        return h.detach()
