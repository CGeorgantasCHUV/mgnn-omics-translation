import numpy as np
import networkx as nx
import itertools
from sklearn.metrics.pairwise import rbf_kernel
from sklearn.neighbors import NearestNeighbors


def compute_similarity_graph_with_knn(df, threshold=0.5, neighbors=1):
    indexes = df.index
    n_nodes = len(indexes)  # one node for each patient
    graph = nx.empty_graph(n_nodes)
    sim_mat = rbf_kernel(df, gamma=0.5 / df.shape[1])
    # compute edges based on distance
    for edge1, edge2 in itertools.combinations(indexes, 2):
        dist = sim_mat[indexes.get_loc(edge1), indexes.get_loc(edge2)]  # symmetric
        if dist > threshold and edge1 != edge2:
            graph.add_edge(indexes.get_loc(edge1), indexes.get_loc(edge2), weight=dist)

    # add edges for isolates
    isolates = np.array(list(nx.isolates(graph)))
    if neighbors > 0:
        nbrs = NearestNeighbors(n_neighbors=neighbors + 1, algorithm="auto").fit(
            df
        )  # add 1 to take same sample into acount
        knn_distances, knn_indices = nbrs.kneighbors(df)
        for i, neighs in enumerate(knn_indices):
            if i in isolates:
                for n in neighs[1:]:  # first neighbors is the same node
                    graph.add_edge(i, n, weight=threshold)

    return graph


def _find_dominate_set(W, K=20):
    """
    from https://github.com/rmarkello/snfpy/blob/master/snf/compute.py
    Retains `K` strongest edges for each sample in `W`
    Parameters
    ----------
    W : (N, N) array_like
        Input data
    K : (0, N) int, optional
        Number of neighbors to retain. Default: 20
    Returns
    -------
    Wk : (N, N) np.ndarray
        Thresholded version of `W`
    """

    # let's not modify W in place
    Wk = W.copy()

    # determine percentile cutoff that will keep only `K` edges for each sample
    # remove everything below this cutoff
    cutoff = 100 - (100 * (K / len(W)))
    Wk[Wk < np.percentile(Wk, cutoff, axis=1, keepdims=True)] = 0

    # normalize by strength of remaining edges
    Wk = Wk / np.nansum(Wk, axis=1, keepdims=True)

    return Wk


def compute_similarity_graph_SNF(df, K=20, mu=0.5):
    # from https://github.com/rmarkello/snfpy/blob/master/snf/compute.py
    from scipy.spatial.distance import cdist
    from scipy import sparse, stats

    arr = df.to_numpy()
    dist = cdist(arr, arr)
    T = np.sort(dist, axis=1)
    TT = np.vstack(T[:, 1 : K + 1].mean(axis=1) + np.spacing(1))

    # compute sigma (see equation in Notes)
    sigma = (TT + TT.T + dist) / 3
    msigma = np.ma.array(sigma)  # mask for NaN
    sigma = sigma * np.ma.greater(msigma, np.spacing(1)).data + np.spacing(1)

    # get probability density function with scale = mu*sigma and symmetrize
    scale = mu * np.nan_to_num(sigma)
    W = stats.norm.pdf(np.nan_to_num(dist), loc=0, scale=scale)
    W = _find_dominate_set(W, K)
    graph = nx.from_numpy_matrix(W)
    return graph


def compute_similarity_graph(df, threshold=0.1):
    indexes = df.index
    n_nodes = len(indexes)  # one node for each patient
    graph = nx.empty_graph(n_nodes)
    sim_mat = rbf_kernel(df, gamma=0.5 / df.shape[1])

    for edge1, edge2 in itertools.combinations(indexes, 2):
        dist = sim_mat[indexes.get_loc(edge1), indexes.get_loc(edge2)]  # symmetric
        if dist > threshold and edge1 != edge2:
            graph.add_edge(indexes.get_loc(edge1), indexes.get_loc(edge2), weight=dist)

    return graph


def generate_edgelist(graph, data="weight"):
    return list(graph.edges(data=data))


if __name__ == "__main__":
    pass
