import numpy as np
import pickle as pkl
import scipy.sparse as sp


def generate_indexes(n_nodes, n_train=100, n_val=100):
    idx = np.arange(0, n_nodes)
    train_idx = np.random.choice(idx, n_train, replace=False)
    val_idx = np.random.choice(np.delete(idx, train_idx), n_val, replace=False)
    test_idx = np.delete(idx, np.concatenate((train_idx, val_idx)))
    return train_idx, val_idx, test_idx


def process_dict(data: dict, sc=3):
    adj_list = [sp.csr_matrix(x + np.eye(x.shape[0]) * sc) for x in data["adj_list"]]
    adj_list_train = [
        sp.csr_matrix(x + np.eye(x.shape[0]) * sc) for x in data["adj_list_train"]
    ]
    label = data["labels"]
    features = sp.lil_matrix(data["features"].astype(float))

    idx_train = data["train_idx"].ravel()
    idx_val = (
        data["val_idx"].ravel()
        if data["val_idx"] is not None
        else data["test_idx"].ravel()
    )
    idx_test = data["test_idx"].ravel()

    return (adj_list, features, label, idx_train, idx_val, idx_test), adj_list_train


def load_tcga(sc=3, path="data/TCGA/TCGA_graph.pkl"):
    data = pkl.load(open(path, "rb"))
    adj_list = [sp.csr_matrix(x + np.eye(x.shape[0]) * sc) for x in data["adj_list"]]
    if "adj_list_train" in data.keys():
        adj_list_train = [
            sp.csr_matrix(x + np.eye(x.shape[0]) * sc) for x in data["adj_list_train"]
        ]
    else:
        adj_list_train = None
    label = data["labels"]

    features = sp.lil_matrix(data["features"].astype(float))

    idx_train = data["train_idx"].ravel()
    idx_val = data["val_idx"].ravel()
    idx_test = data["test_idx"].ravel()

    return adj_list, features, label, idx_train, idx_val, idx_test, adj_list_train


if __name__ == "__main__":
    pass
