from sklearn import cluster
import numpy as np
from graphs.preprocessing import (
    compute_similarity_graph_SNF,
    compute_similarity_graph_with_knn,
)
import networkx as nx


def cluster_features(df, cluster_args):
    X = df.to_numpy()
    agglo = cluster.FeatureAgglomeration(**cluster_args)
    # distance_threshold=df.shape[0] * threshold, n_clusters=None

    agglo.fit(X)
    clusters = agglo.labels_
    return clusters


class GraphConstructorSNF:
    """
    take dataframe, return multiplex graph in the form of a list of nx graphs
    """

    def __init__(self) -> None:
        pass

    def _process(self, df):
        graph = compute_similarity_graph_SNF(df)
        return graph

    def to_adj_dict(self):
        adj_dict = {}
        for key, graph_list in self.graph_dict.items():
            adj_list = []
            for graph in graph_list:
                adj_list.append(nx.linal.graphmatrix.adjacency_matrix(graph))
            adj_dict[key] = adj_list
        return adj_dict

    def __call__(self, df):
        df_mod1 = df.iloc[
            :, 0:6000
        ]  # TODO: hard coded feature splits, this should be changed
        df_mod2 = df.iloc[:, 6000:-1]

        return [self._process(df_mod1), self._process(df_mod2)]


class FeatureClusterGraphConstructor:
    """
    take dataframe, return multiplex graph in the form of a list of nx graphs
    """

    def __init__(
        self, train_idx=None, n_clusters=None, cluster_threshold=1, sim_threshold=0.5
    ) -> None:
        if not n_clusters:
            cluster_args = dict(distance_threshold=cluster_threshold, n_clusters=None)
        else:
            cluster_args = dict(n_clusters=n_clusters)
        self.cluster_args = cluster_args
        self.train_idx = train_idx
        self.graphs = None
        self.sim_threshold = sim_threshold

    def generate_clusters(self, df):
        if self.train_idx is not None:
            clusters = cluster_features(
                df.loc[df.index[self.train_idx]], self.cluster_args
            )
        else:
            clusters = cluster_features(df, self.cluster_args)
        return clusters

    def _process(self, df, clusters):
        graphs = []
        n_clusters = len(np.unique(clusters))
        for i in range(n_clusters):
            sub_df_x = df.loc[:, df.columns[clusters == i]]
            graph = compute_similarity_graph_with_knn(
                sub_df_x, threshold=self.sim_threshold, neighbors=10
            )
            graphs.append(graph)
        return graphs

    def to_adj(self):
        adj_list = []
        if not self.graphs:
            self.graphs = self._process()
        for graph in self.graphs:
            adj_list.append(nx.linal.graphmatrix.adjacency_matrix(graph))
        return adj_list

    def __call__(self, df):
        clusters = self.generate_clusters(df)
        return self._process(df, clusters)


def build_graph_data(
    graphs, features, labels, train_idx, test_idx, val_idx=None, graphs_train=None
):
    """build a multiplex graph dataset from dataframe"""

    out_dict = {}
    out_dict["adj_list"] = [
        nx.linalg.graphmatrix.adjacency_matrix(g).todense() for g in graphs
    ]
    if graphs_train is not None:
        out_dict["adj_list_train"] = [
            nx.linalg.graphmatrix.adjacency_matrix(g).todense() for g in graphs_train
        ]
    out_dict["features"] = features
    out_dict["labels"] = labels
    out_dict["train_idx"] = train_idx
    out_dict["val_idx"] = val_idx
    out_dict["test_idx"] = test_idx

    return out_dict
