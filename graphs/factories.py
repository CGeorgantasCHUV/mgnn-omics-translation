import torch
from graphs import loaders
import graphs.utils as utils


class GraphDatasetFactory:
    @classmethod
    def from_dict(cls, data, args):
        outs, adj_list_train = loaders.process_dict(data, sc=args.sc)
        return cls(*outs, args.device, adj_list_train=adj_list_train)

    @classmethod
    def from_tcga(cls, args, path=None):
        if path:
            outs = loaders.load_tcga(args.sc, path)
        else:
            outs = loaders.load_tcga(args.sc)
        return cls(*outs, args.device)

    def __init__(
        self,
        adj_list,
        features,
        labels,
        idx_train,
        idx_val,
        idx_test,
        device,
        adj_list_train=None,
    ):

        features = features.todense()

        self.nb_nodes = adj_list[0].shape[0]
        self.ft_size = features[0].shape[1]
        self.nb_classes = labels.shape[1]

        adj_list = [utils.normalize_adj(adj) for adj in adj_list]
        self.adj_list = [
            utils.sparse_mx_to_torch_sparse_tensor(adj) for adj in adj_list
        ]
        if adj_list_train is not None:
            adj_list_train = [utils.normalize_adj(adj) for adj in adj_list_train]
            self.adj_list_train = [
                utils.sparse_mx_to_torch_sparse_tensor(adj) for adj in adj_list_train
            ]
        else:
            self.adj_list_train = None
        self.features = torch.FloatTensor(features)
        self.labels = torch.FloatTensor(labels).to(device)
        self.idx_train = torch.LongTensor(idx_train).to(device)
        self.idx_val = torch.LongTensor(idx_val).to(device)
        self.idx_test = torch.LongTensor(idx_test).to(device)
