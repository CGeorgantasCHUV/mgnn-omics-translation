code for the article Multi-view Omics Translation with Multiplex Graph Neural Networks

## License
This source code is licensed under the [MIT](https://github.com/zhangxiaoyu11/OmiTrans/blob/master/LICENSE) license.
